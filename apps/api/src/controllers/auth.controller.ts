import crypto from 'node:crypto';
import { PrismaClient } from '@prisma/client';
import type { Request, Response, NextFunction } from 'express';
import { hash, verify } from 'argon2';
import { HttpException } from '../exceptions';
import { redis } from '../lib';

const prisma = new PrismaClient();

// TODO: move types to package
interface RegisterUserProps {
  name: string;
  email: string;
  password: string;
  role: 'USER' | 'REALTOR' | 'ADMIN';
}

type LoginUserProps = Pick<RegisterUserProps, 'email' | 'password'>;
type ForgotUserPasswordProps = Pick<LoginUserProps, 'email'>;
type ResetUserPasswordProps = Pick<LoginUserProps, 'password'> & {
  confirm_password: string;
};

export async function registerUser(
  req: Request<unknown, unknown, RegisterUserProps>,
  res: Response,
  next: NextFunction,
) {
  try {
    const hashed = await hash(req.body.password); // argon2 hash

    const user = await prisma.user.create({
      data: {
        ...req.body,
        password: hashed,
      },
    });

    const { password: _password, ...userDetails } = user;

    return res.status(201).json(userDetails);
  } catch (error) {
    next(error);
  }
}

export async function loginUser(
  req: Request<unknown, unknown, LoginUserProps>,
  res: Response,
  next: NextFunction,
) {
  try {
    const user = await prisma.user.findUnique({
      where: {
        email: req.body.email,
      },
    });

    if (!user) return new HttpException(401, 'Invalid credentials');

    // verify password
    const isPasswordVerified = await verify(user.password, req.body.password);

    if (!isPasswordVerified)
      return new HttpException(401, 'Invalid credentials');

    const { password: _password, ...userDetails } = user;

    return res.status(200).json(userDetails);
  } catch (error) {
    next(error);
  }
}

export async function forgotUserPassword(
  req: Request<unknown, unknown, ForgotUserPasswordProps>,
  res: Response,
  next: NextFunction,
) {
  try {
    const user = await prisma.user.findUnique({
      where: {
        email: req.body.email,
      },
    });

    // return true regardless, but if user exists, send email, save code in redis
    let userDetails;
    if (user) {
      const { password: _password, ...fpUser } = user;
      userDetails = fpUser;
      const randomCode = crypto.randomBytes(32).toString('hex');
      await redis.set(randomCode, user.email, 'EX', 60 * 60 * 24); // expire after 24hrs
    }

    return res.status(200).json(userDetails);
  } catch (error) {
    next(error);
  }
}

export async function resetUserPassword(
  req: Request<{ confirmation_code: string }, unknown, ResetUserPasswordProps>,
  res: Response,
  next: NextFunction,
) {
  try {
    // get email from redis
    const confirmationCode = req.query.confirmation_code;

    if (!confirmationCode)
      return new HttpException(400, 'Confirmation code is required');

    const email = await redis.get(confirmationCode as string);

    if (!email) return new HttpException(404, 'Confirmation code expired');

    const user = await prisma.user.update({
      where: {
        email,
      },
      data: {
        ...req.body,
      },
    });

    const { password: _password, ...userDetails } = user;

    return res.status(200).json(userDetails);
  } catch (error) {
    next(error);
  }
}

export async function googleOAuthLogin(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  // input: TBD
  return null;
}
