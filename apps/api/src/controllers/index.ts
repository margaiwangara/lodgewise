export {
  registerUser,
  loginUser,
  forgotUserPassword,
  resetUserPassword,
  googleOAuthLogin,
} from './auth.controller';
