import { config } from 'dotenv'; // TODO: fix errors
import { cleanEnv, str } from 'envalid';

// init dotenv
config();

const env = cleanEnv(process.env, {
  NODE_ENV: str({ default: 'dev', choices: ['dev', 'prod', 'test'] }),

  CLIENT_URL: str({ default: 'http://localhost:3000' }),
  REDIS_USERNAME: str(),
  REDIS_PASSWORD: str(),
  DATABASE_URL: str(),
});

export default env;
