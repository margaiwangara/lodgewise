import Redis from 'ioredis';
import { log } from 'logger';
import env from '../env';

const redis = new Redis({
  port: 3302,
  username: env.REDIS_USERNAME,
  password: env.REDIS_PASSWORD,
});

redis.on('error', (error) => log('Redis Error: ', error));

export { redis };
