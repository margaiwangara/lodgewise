import type { NextFunction, Request, Response } from 'express';
import type { HttpException } from '../exceptions';

export function errorHandler(
  error: HttpException,
  req: Request,
  res: Response,
  _next: NextFunction,
) {
  return res.status(error.status || 500).json({
    error: {
      status: error.status || 500,
      message: error.message || 'Oops! Something went wrong',
    },
  });
}
