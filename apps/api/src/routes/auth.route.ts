import { Router } from 'express';
import {
  loginUser,
  registerUser,
  googleOAuthLogin,
  forgotUserPassword,
  resetUserPassword,
} from '../controllers';

const router = Router(); // TODO: fix error

// TODO: fix errors
router.post('/login', loginUser);
router.post('/register', registerUser);
router.post('/forgot-password', forgotUserPassword);
router.post('/reset-password', resetUserPassword);
router.post('/google', googleOAuthLogin);

export { router };
