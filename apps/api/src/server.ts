import { json, urlencoded } from 'body-parser';
import express, { type Express } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import env from './env';
import { errorHandler } from './middlewares';
import { HttpException } from './exceptions';

export const createServer = (): Express => {
  const app = express();

  app
    .disable('x-powered-by')
    .use(morgan(env.NODE_ENV))
    .use(urlencoded({ extended: true }))
    .use(json())
    .use(
      cors({
        credentials: true,
        origin: [env.CLIENT_URL],
      }),
    )
    .get('/', (_req, res) => res.send('v1.lodgewise.com'))
    .use((_req, _res, next) => {
      const error = new HttpException(404, 'Not Found');
      next(error);
    })
    .use(errorHandler);

  return app;
};
