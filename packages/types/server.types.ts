import type { Request } from 'express';
import type { Redis } from 'ioredis';

export interface ExpressRequestProps extends Request {
  redis: Redis;
}
